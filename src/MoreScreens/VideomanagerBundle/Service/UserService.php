<?php
/**
 * Created by PhpStorm.
 * User: Haris
 * Date: 7/1/14
 * Time: 9:46 PM
 */

namespace morescreens\VideomanagerBundle\Service;


use morescreens\VideomanagerBundle\Entity\User;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;


class UserService {


    private $encoderFactory;



    public function  __construct( EncoderFactoryInterface $encoderFactory)
    {
           $this->encoderFactory=$encoderFactory;
    }


    public  function  setEncodedPassword(User $user)
   {
         $encoder=$this->encoderFactory->getEncoder($user);
         $password=$encoder->encodePassword($user->getPassword(),$user->getSalt());
         $user->setPassword($password);
   }

}

?>