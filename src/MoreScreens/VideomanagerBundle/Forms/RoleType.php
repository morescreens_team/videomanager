<?php
/**
 * Created by PhpStorm.
 * User: Haris
 * Date: 7/3/14
 * Time: 11:13 AM
 */

namespace morescreens\VideomanagerBundle\Forms;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RoleType extends AbstractType {
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $form->add('role',"choice",array(
                "choices"=>array("admin"=>"USER_ADMIN","user"=>"USER_USER"),
                "label"=>"Odaberite ulogu"
            ));

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
       $resolver->setDefaults(array('data_class'=>'morescreens\VideomanagerBundle\Entity\Role'));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
       return "Role";
    }


} 