<?php
/**
 * Created by PhpStorm.
 * User: Haris
 * Date: 6/30/14
 * Time: 3:54 AM
 */

namespace morescreens\VideomanagerBundle\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;

/**
 * Class Video
 * @package morescreens\VideomanagerBundle\Entity
 * @Entity()
 * @Table(name="Videos")
 */
class Video {
    /**
     * @Id()
     * @Column(name="video_id",type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @Column(name="name",type="string",length=250,nullable=false)
     */
    private $name;


    /**
     * @Column(name="url",type="string",length=250,nullable=false)
     */

    private $url;
    /**
     * @Column(name="date_add",type="date",nullable=false)
     */
    private $dateAdd;
    /**
     * @Column(name="time_add",type="time",nullable=false)
     */
    private $timeAdd;
    /**
     * @Column(name="thumbnail",type="string",length=100,nullable=false)
     */
    private $thumbnail;
    /**
     * @ManyToOne(targetEntity="User",inversedBy="Video",cascade={"persist"})
     * @JoinColumn(name="user_id",referencedColumnName="user_id")
     */
    private $user;
    /**
     * @ManyToMany(targetEntity="Category")
     * @JoinTable(name="video_category",
     *      joinColumns={@JoinColumn(name="video_id", referencedColumnName="video_id")},
     *      inverseJoinColumns={@JoinColumn(name="category_id", referencedColumnName="category_id")}
     *      )
     */
    private $category;



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->category = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Video
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Video
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set dateAdd
     *
     * @param \DateTime $dateAdd
     * @return Video
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd
     *
     * @return \DateTime 
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }

    /**
     * Set timeAdd
     *
     * @param \DateTime $timeAdd
     * @return Video
     */
    public function setTimeAdd($timeAdd)
    {
        $this->timeAdd = $timeAdd;

        return $this;
    }

    /**
     * Get timeAdd
     *
     * @return \DateTime 
     */
    public function getTimeAdd()
    {
        return $this->timeAdd;
    }

    /**
     * Set thumbnail
     *
     * @param string $thumbnail
     * @return Video
     */
    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    /**
     * Get thumbnail
     *
     * @return string 
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * Set user
     *
     * @param \morescreens\VideomanagerBundle\Entity\User $user
     * @return Video
     */
    public function setUser(\morescreens\VideomanagerBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \morescreens\VideomanagerBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add category
     *
     * @param \morescreens\VideomanagerBundle\Entity\Category $category
     * @return Video
     */
    public function addCategory(\morescreens\VideomanagerBundle\Entity\Category $category)
    {
        $this->category[] = $category;

        return $this;
    }

    /**
     * Remove category
     *
     * @param \morescreens\VideomanagerBundle\Entity\Category $category
     */
    public function removeCategory(\morescreens\VideomanagerBundle\Entity\Category $category)
    {
        $this->category->removeElement($category);
    }

    /**
     * Get category
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategory()
    {
        return $this->category;
    }
}
