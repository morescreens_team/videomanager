<?php
/**
 * Created by PhpStorm.
 * User: Haris
 * Date: 6/30/14
 * Time: 3:55 AM
 */

namespace morescreens\VideomanagerBundle\Entity;


use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\Table;


/**
 * Class Category
 * @package morescreens\VideomanagerBundle\Entity
 * @Entity()
 * @Table(name="Categories")
 */
class Category {
    /**
     * @Id()
     * @Column(name="category_id",type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
      protected $id;
    /**
     * @Column(name="category_name",type="string",length=100)
     */
      protected $name;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     */
    protected $video;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
}
