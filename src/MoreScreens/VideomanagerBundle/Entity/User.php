<?php
/**
 * Created by PhpStorm.
 * User: Haris
 * Date: 6/27/14
 * Time: 2:06 AM
 */

namespace morescreens\VideomanagerBundle\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;


/**
 *
 * @Entity()
 * @Table(name="Users")
 *
 */
class User implements UserInterface,\Serializable
{
    /**
     * @Id()
     * @Column(name="user_id",type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @Column(name="username",type="string",length=100,nullable=false)
     */
    protected $username;
    /**
     * @Column(name="password",type="string",length=250,nullable=false)
     */
    protected $password;
    /**
     * @Column(name="email",type="string",length=100,nullable=false)
     */

    protected $email;
    /**
     * @Column(name="firstname",type="string",length=100,nullable=true)
     */
    protected $firstname;
    /**
     * @Column(name="lastname",type="string",length=100,nullable=true)
     */
    protected $lastname;
    /**
     * @ManyToOne(targetEntity="Role",inversedBy="User",cascade={"persist"})
     * @JoinColumn(name="role_id" , referencedColumnName="role_id")
     */
    protected $roles;

  /*  public function __construct()
    {
       $this->roles = new ArrayCollection();
    }
*/

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set role
     *
     * @param \morescreens\VideomanagerBundle\Entity\Role $role
     * @return User
     */
    public function setRoles(\morescreens\VideomanagerBundle\Entity\Role $role = null)
    {
        $this->roles=$role;

        return $this;
    }

    /**
     * Get role
     *
     * @return \morescreens\VideomanagerBundle\Entity\Role
     * @return array
     */
    public function getRoles()
    {
        return array($this->roles);
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     */
    public function serialize()
    {
        return serialize(array(
                $this->id,
                $this->username,
                $this->password,
                $this->email,
                $this->lastname,
                $this->firstname,
                // see section on salt below
                // $this->salt,
            ));
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            $this->email,
            $this->lastname,
            $this->firstname

            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }

    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return Role[] The user roles
     */


    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        // TODO: Implement getSalt() method.
          return null;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }
}
