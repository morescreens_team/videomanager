<?php
/**
 * Created by PhpStorm.
 * User: Haris
 * Date: 6/27/14
 * Time: 2:23 AM
 */

namespace morescreens\VideomanagerBundle\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use Symfony\Component\Security\Core\Role\RoleInterface;

/**
 * Class Role
 * @package morescreens\VideomanagerBundle\Entity
 * @Entity()
 * @Table(name="Roles")
 */
class Role implements RoleInterface , \Serializable
{
    /**
     * @Id()
     * @Column(name="role_id",type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @Column(name="role",type="string",length=100)
     */
    protected $role;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set role
     *
     * @param string $role
     * @return Role
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string 
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     */
    public function serialize()
    {
       return serialize
       (array(
           $this->id,
           $this->role
            )

       );
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->role,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }
}
