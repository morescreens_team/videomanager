<?php

namespace morescreens\VideomanagerBundle\Controller;

use morescreens\VideomanagerBundle\Entity\Role;
use morescreens\VideomanagerBundle\Entity\User;
use morescreens\VideomanagerBundle\Forms\RoleType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\SecurityContext;

class DefaultController extends Controller
{
    public function indexAction($name)
    {

      $role=new Role();
      $form=$this->createForm(new RoleType(),$role);


        return $this->render('morescreensVideomanagerBundle:Default:index.html.twig', array('form' => $form->createView()));
    }


    public  function loginAction()
    {

        $request = $this->getRequest();
        $session = $request->getSession();
// get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                SecurityContext::AUTHENTICATION_ERROR
            );
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }
        echo $error;

        return $this->render('morescreensVideomanagerBundle:Default:login.html.twig');
    }

    public  function adminAction()
    {
        $user=new User();
        $user->setPassword('testvgenerate entities cannot find base path');

        $this->get('user_service')->setEncodedPassword($user);


       return new Response($user->getPassword());
    }
}
